<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\Consultation;
use App\Mail\Purchase;
use App\Mail\Sale;

class MailingController extends Controller {


    public function consultation(Request $request) {
        // sleep(5);
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'array',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_TO_ADDRESS'))->send(new Consultation($request));


        return [ 'status' => 'success', 'message' => 'Сообщение успешно отправлено. Я свяжусь с Вами в ближайшее время.' ];
    }



    public function purchase(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'array',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_TO_ADDRESS'))->send(new Purchase($request));


        return [ 'status' => 'success', 'message' => 'Заявка успешно отправлена. Я свяжусь с Вами в ближайшее время.' ];
    }



    public function sale(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            // 'email' => 'array',
            'phone' => 'required',
            'message' => 'required',
        ]);

        Mail::to(env('MAIL_TO_ADDRESS'))->send(new Sale($request));


        return [ 'status' => 'success', 'message' => 'Заявка успешно отправлена. Я свяжусь с Вами в ближайшее время.' ];
    }



}
