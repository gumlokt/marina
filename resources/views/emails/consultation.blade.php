<!DOCTYPE html>
<html>
    <head>
        <title>{{ isset($form->name) ? $form->name : 'Поле Имя не заполнено...' }}</title>
    </head>
    <body>
        <h3>Заявка на консультацию</h3>
        
        <dl>
            <dt>Имя:</dt>
            <dd>{{ isset($form->name) ? $form->name : 'Поле Имя не заполнено...' }}</dd>
            
            <dt>Email:</dt>
            <dd>{{ isset($form->email) ? $form->email : 'Поле Email не заполнено...' }}</dd>
            
            <dt>Телефон:</dt>
            <dd>{{ isset($form->phone) ? $form->phone : 'Поле Телефон не заполнено...' }}</dd>
            
            <dt>Сообщение:</dt>
            <dd>{{ isset($form->message) ? $form->message : 'Поле Сообщение не заполнено...' }}</dd>
        </dl>


        <br>
        <hr>
        
        <em>Данное сообщение пришло Вам через форму обратной связи: http://89533801100.рф/contacts</em>
    </body>
</html>
