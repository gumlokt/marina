@extends('layouts.app')

@section('content')

	@include('sections.home')
	@include('sections.realtor')
	@include('sections.intro')


	@include('sections.partners')
	@include('sections.testimonials')
	@include('sections.gotop')


@endsection
