        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-sm navbar-light bg-light shadow">
            <div class="container">



                <a class="navbar-brand" href="{{ url('/') }}" title="Морозова Марина. Эксперт по недвижимости. Сертификат №РОСТ RU РГРТОС 66БН2525">
                    <!-- <img src="{{ asset('images/logo.png') }}" height="30" class="d-inline-block align-top" alt=""> -->
                    <span class="text-light">
                        <i class="fas fa-home fa-fw text-danger"></i> Главная
                    </span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <!-- <a class="nav-link text-light" href="{{ url('login') }}" title="Морозова Марина. Эксперт по недвижимости. Сертификат №РОСТ RU РГРТОС 66БН2525">
                                <i class="far fa-file-alt fa-fw text-danger"></i> Устав
                            </a> -->
                        </li>
                    </ul>


                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/about') }}">
                                <i class="fas fa-info fa-fw text-danger"></i> Обо мне
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light" href="{{ url('/contacts') }}">
                                <i class="far fa-envelope fa-fw text-danger"></i> Контакты
                            </a>
                        </li>
                    </ul>
                </div>


            </div>
        </nav>
