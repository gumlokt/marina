@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-exchange-alt fa-sm"></i> Обмен
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<h3>
						<i class="far fa-question-circle"></i> Допускаете ли Вы для себя сценарий с обменом?
					</h3>

					<h5>
						<i class="fas fa-check"></i> Обмен недвижимости — задача непростая. Это сделка, в которой сочетаются встречные операции: продажа имеющегося объекта и покупка другого, иногда с оформлением ипотеки или расчетом сертификатами. Несмотря на то, что прямые обмены сейчас редкость, я достаточно часто ими пользуюсь. Это позволяет уменьшить затраты и сократить сроки, что выгодно для моих клиентов.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Очень важно подобрать хороший встречный вариант, проверить все документы, согласовать все условия, в том числе вопрос одновременной продажи и покупки. Тем самым мы минимизируем риски.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Мои клиенты часто благодарят меня за то, что при обмене они получают большую площадь квартиры, чем рассчитывали. Ведь дополнительная комната никому не лишняя.
					</h5>

					<h5>
						<div>Если вы предполагаете обменять жилье на большее, или необходим разъезд, в том числе и междугородний – смело звоните. Я приложу все усилия, чтобы у нас с Вами все получилось!</div>
						<br>
						<div>
							<strong>Звоните</strong>, не откладывая, по телефону: <strong class="text-primary"><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
						</div>
						<div>
							<i>Звонки принимаются: <i class="far fa-clock text-danger"></i> с <strong class="text-primary">7:30</strong> до <strong class="text-primary">23:00</strong></i>.
						</div>
					</h5>


				</div>
			</div>
		</div>
    </section>
@endsection
