@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-shield-alt fa-sm"></i> Безопасная сделка
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<h3>
						<i class="far fa-question-circle"></i> Для чего нужна проверка квартиры для купли-продажи?
					</h3>

					<h5>
						<i class="fas fa-check"></i> Выяснить безопасность приобретения выбранного Вами жилья – вот задача проверки. Ведь, заключая сделку, важно не только приобрести, но и, впоследствии, не потерять! Именно поэтому юридическая проверка квартиры – серьезное и необходимое действие.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Бытует ошибочное мнение, что покупая квартиру в ипотеку, покупатель полностью застрахован от всех рисков, т.к. банк берет на себя ответственность за проверку приобретаемого объекта. Однако, на практике, банк проверяет лишь последний переход права, и страхует средствами заемщика свои риски. В случае, если квартира будет утрачена или оспорена третьими лицами, банк возместит свои потери за счет страховой компании, или за счет средств заемщика. Всегда проверяйте квартиру, чтобы избежать в последствии судебных тяжб.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Если сделка заверяется нотариусом, значит она будет законной и никто не сможет ее оспорить? Это совсем не так. При нотариальном заверении, нотариус лишь подтверждает, что подписи в договоре поставили именно те люди, которые указаны в качестве сторон договора. Иными словами, он гарантирует удостоверение личностей сторон и их волю на заключение данной сделки. Однако, он не проверяет сделку на наличие оснований, по которым сделка может быть признана недействительной, не проверяет сам договор и не будет указывать Вам на возможный риск при подписании договора.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Думаете, что достаточно просмотреть выписку из Единого Государственного Реестра Недвижимости на объект недвижимости, чтобы удостовериться, что сделке ничего не грозит? Выписка из Единого Государственного Реестра Недвижимости на объект недвижимости, безусловно, важный и информативный документ, однако существует одно «Но»! Если её показывает Вам продавец либо агентство, не следует доверять данному документу. Он достаточно легко подделывается - ведь выписка печатается на обычной бумаге и не защищается водяными знаками или какими-либо иными степенями защиты. Как подпись, так и печать на выписке легко подделываются мошенниками. Несмотря на то, что данные деяния являются уголовными преступлениями, наказанием за них является штраф в размере не более 120 тыс. рублей, что, согласитесь, несоразмерно со стоимостью квартиры.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Покупка квартиры ранее бывшей в ипотеке, т.е. в залоге у банка, также несет в себе риски. Зачастую кредит частично погашается средствами по сертификату Материнского капитала, а дети собственностью не наделяются, что в дальнейшем ведет к оспоримости сделки или возникновению права проживания сторонних лиц в Вашей квартире.
					</h5>

					<h5>
						<i class="fas fa-check"></i> В настоящее время одной из самых главных проблем, которую принес Федеральный закон от 26.10.2002 N 127-ФЗ «О несостоятельности (банкротстве)» для покупателей недвижимости является следующее. Если сделку с недвижимостью совершил продавец – банкрот, арбитражный суд может признать недействительной даже спустя 3 года после ее совершения. При этом гражданин может и не быть банкротом на момент совершения сделки, а объявить себя таковым после. Но и эта задача вполне решаема – главное знать как!
					</h5>

					<h5>
						<div>Существует ещё много других всевозможных рисков. Хотите купить безопасно? <strong>Я Вам обязательно помогу</strong>!</div>
						<br>
						<div>
							Звоните, не откладывая, по телефону: <strong class="text-primary"><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
						</div>
						<div>
							<i>Звонки принимаются: <i class="far fa-clock text-danger"></i> с <strong class="text-primary">7:30</strong> до <strong class="text-primary">23:00</strong></i>.
						</div>
					</h5>

				</div>
			</div>
		</div>
    </section>
@endsection
