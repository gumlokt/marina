@extends('layouts.app')

@section('content')
	<section class="animated" data-section="contacts" id="marina-contacts">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="far fa-envelope fa-sm"></i> Контакты
					</h2>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-6 to-animate fadeInUp animated">
					<h3>Мои контакты</h3>
					<ul class="marina-contacts-info">
						<li><i class="far fa-smile fa-fw"></i> Морозова Марина Геннадьевна</li>
						<li class="marina-contacts-address "><i class="fas fa-home fa-fw"></i> ...</li>
						<li><i class="fas fa-mobile-alt fa-fw"></i> +7 953 380 11 00</li>
						<li><i class="far fa-envelope fa-fw"></i> marina_morozova79@mail.ru</li>
						<li><i class="fas fa-globe fa-fw"></i> <a href="http://89533801100.рф/" target="_blank">89533801100.рф</a></li>
					</ul>
				</div>

				<div class="col-md-6 to-animate fadeInUp animated">
					<h3>Закажите бесплатную консультацию</h3>

					<input type="hidden" name="url" value="/mail/consultation">

					<div class="form-group ">
						<label for="name" class="sr-only">Имя</label>
						<input name="name" class="form-control" placeholder="*Имя" type="text">
					</div>

					<div class="form-group ">
						<label for="email" class="sr-only">Email</label>
						<input name="email" class="form-control" placeholder="Email" type="email">
					</div>

					<div class="form-group ">
						<label for="phone" class="sr-only">Телефон</label>
						<input name="phone" class="form-control" placeholder="*Телефон" type="text">
					</div>

					<div class="form-group ">
						<label for="message" class="sr-only">Сообщение</label>
						<textarea name="message" cols="30" rows="5" class="form-control" placeholder="*Сообщение"></textarea>
					</div>

					<div class="form-group ">
						<button class="btn btn-primary btn-lg"  type="submit" id="sendBtn">
							Отправить <i class="far fa-paper-plane"></i>
						</button>
					</div>
					<small class="text-danger" style="margin-left: 20px;" id="statusMessage">{{ (isset($status) and !empty($status)) ? $status : '' }}</small>
				</div>
			</div>
	</section>
@endsection
