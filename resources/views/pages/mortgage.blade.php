@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-money-bill fa-sm"></i> Выгодная ипотека
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<h3>
						<i class="far fa-question-circle"></i> Вы подобрали хорошую квартиру, а на покупку не хватает средств?
					</h3>

					<h5>
						<i class="fas fa-check"></i> Для Вас я согласую ипотеку дешевле, чем при Вашем личном обращении в банк. За счет партнерской программы банки снижают ставку по кредиту для моих клиентов. Разница может составить <strong>от 0,25 до 0,6%</strong>, что даст Вам <strong>экономию свыше 200 тыс.руб</strong>.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Помогу Вам собрать необходимый пакет документов.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Вы предоставляете документы <strong>«в одно окно»</strong>. А я, в свою очередь, отправляю их в 3-5 банков, в зависимости от Вашей ситуации. Затем мы вместе выбираем наиболее выгодные условия с учетом предложенных ставок, графика платежей, условий погашения и других параметров.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Практически в каждом банке я пользуюсь услугами личных менеджеров или профессионального онлайн-кабинета, это позволяет существенно сократить сроки рассмотрения заявки на кредит.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Не хватает денег на первоначальный взнос? С моей помощью эта задача решается легко!
					</h5>

					<h5>
						<i class="fas fa-check"></i> Получаете «зарплату в конверте» и не можете предоставить справку 2-НДФЛ? Я знаю как справиться и с этой ситуацией - <strong>оформление возможно</strong>!
					</h5>

					<h5>
						<i class="fas fa-check"></i> Если заявка на ипотеку Вами уже оформлена, я помогу согласовать выбранную квартиру, независимо от перепланировок.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я помогу снизить Вам расходы на страховку почти <strong>в 2 раза</strong>!
					</h5>

					<h5>
						<div>Я знаю как выгодно оформить ипотеку с минимальными платежами при минимальных затратах!</div>
						<br>
						<div>
							<strong>Звоните</strong>, не откладывая, по телефону: <strong class="text-primary"><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
						</div>
						<div>
							<i>Звонки принимаются: <i class="far fa-clock text-danger"></i> с <strong class="text-primary">7:30</strong> до <strong class="text-primary">23:00</strong></i>.
						</div>
					</h5>


				</div>
			</div>
		</div>
    </section>
@endsection
