@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-shopping-bag fa-sm"></i> Покупка
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<h3>
						<i class="far fa-question-circle"></i> Как купить лучшую квартиру из возможных по нужной цене и не переплатить?
					</h3>

					<h5>
						<i class="fas fa-check"></i> САМОЕ ПЕРВОЕ И ГЛАВНОЕ: правильно проанализировать ситуацию, для того чтобы выбрать наилучшее решение, которое может быть осуществлено в этом городе!
					</h5>

					<h5>
						<i class="fas fa-check"></i> На основе Ваших потребностей и возможностей, я проведу анализ рынка предложений, используя как открытые источники (интернет и др.) так и закрытые (профессиональные базы данных).
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я предоставлю Вам самые привлекательные варианты, чтобы Вы могли выбрать лучший из них.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я лично созвонюсь со всеми собственниками и представителями продавцов, уточню особенности квартир, документов и ситуации по продаже, для того чтобы исключить при отборе вариантов ненадежные, рискованные и завышенные по цене предложения.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я отвезу Вас на моем автомобиле, и мы осмотрим лучшие квартиры вместе. Будем учитывать все факторы – ликвидность, удобство, состояние дома и многое другое – о чем Вы пока даже не догадываетесь. Ведь окончательный выбор влияет на качество жизни!
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я проведу профессиональные переговоры и торги с продавцом, СЭКОНОМИВ Вам сумму, гораздо большую чем комиссия за мои услуги.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Проведу анализ документов и юридическую проверку чистоты квартиры и продавцов. Предоставлю Вам полный официальный отчет - конкретные результаты и документы, исключив даже гипотетические риски при покупке.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я возьму на себя всю заботу по юридическому оформлению сделки, подготовлю и соберу необходимые документы, составлю договор купли-продажи квартиры.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Организую проведение сделки, переход права собственности и фактическую передачу квартиры, помогу с переездом и ремонтом (при необходимости).
					</h5>


					<h5>В силу богатого опыта работы и профессионального взгляда на недвижимость – я зачастую в итоге предложу Вам лучшее решение, чем то, о котором Вы мечтали до нашей встречи.</h5>

					<h5>
						<div>Я буду с Вами на связи постоянно – как на период подбора квартиры, так и на протяжении дальнейшей жизни в новой квартире. Я СВОИХ НЕ БРОСАЮ!</div>
						<br>
						<div>
							<strong>Звоните</strong>, не откладывая, по телефону: <strong class="text-primary"><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
						</div>
						<div>
							<i>Звонки принимаются: <i class="far fa-clock text-danger"></i> с <strong class="text-primary">7:30</strong> до <strong class="text-primary">23:00</strong></i>.
						</div>
					</h5>


					<div class="col animate-box fadeInUp animated">
						<div class="price-box">
							<h3>Закажите бесплатную предварительную заявку на подбор квартиры</h3>

							<input type="hidden" name="url" value="/mail/purchase">

							<div class="row justify-content-center">	
								<div class="col-md-6 to-animate fadeInUp animated align-self-center">

									<div class="form-group ">
										<label for="name" class="sr-only">Имя</label>
										<input name="name" class="form-control" placeholder="*Имя" type="text">
									</div>

									<div class="form-group ">
										<label for="email" class="sr-only">Email</label>
										<input name="email" class="form-control" placeholder="Email" type="email">
									</div>

									<div class="form-group ">
										<label for="phone" class="sr-only">Телефон</label>
										<input name="phone" class="form-control" placeholder="*Телефон" type="text">
									</div>

									<div class="form-group ">
										<label for="message" class="sr-only">Сообщение</label>
										<textarea name="message" cols="30" rows="5" class="form-control" placeholder="*Сообщение"></textarea>
									</div>

									<div class="form-group ">
										<button class="btn btn-primary btn-lg"  type="submit" id="sendBtn">
											Заказать <i class="far fa-paper-plane"></i>
										</button>
									</div>
									<small class="text-danger" style="margin-left: 20px;" id="statusMessage">{{ (isset($status) and !empty($status)) ? $status : '' }}</small>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
    </section>
@endsection
