@extends('layouts.app')

@section('content')
	<section class="animated" data-section="usualPage" id="usualPage">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">
						<i class="fas fa-store-alt fa-sm"></i> Продажа
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col section-heading">
					<!-- <h3 class="to-animate fadeInUp animated">Страница находится на стадии наполнения контентом...</h3>
					<button class="btn btn-outline-info to-animate fadeInUp animated" onclick="window.history.back()"><i class="fas fa-angle-double-left"></i> Назад</button> -->

					<h3>
						<i class="far fa-question-circle"></i> Как сберечь и приумножить Ваш финансовый актив - Вашу квартиру?
					</h3>

					<h5>
						<i class="fas fa-check"></i> <strong>САМОЕ ПЕРВОЕ И ГЛАВНОЕ</strong>! Правильно проанализировать ситуацию, для того чтобы выбрать наилучшее решение, которое может быть осуществлено в этом городе.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я охвачу весь рынок продаж аналогичных квартир, проведу для Вас его анализ и на основе этого озвучу максимально возможный диапазон цен продажи квартиры с учётом её особенностей и желаемых сроков продажи.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Для поднятия привлекательности в глазах покупателей я предложу Вам доступные меры по подготовке Вашей квартиры к продаже.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Создам качественное портфолио квартиры (фото-видео-съёмка, описание), выделю её лучшие качества.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Организую масштабную рекламную кампанию - не только в Интернет-пространстве, но и ещё по 6 дополнительным каналам продаж.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Приму все звонки и проведу переговоры, тем самым избавлю Вас от траты времени и сил.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Потенциальным покупателям покажу квартиру ПРОФЕССИОНАЛЬНО. Зачастую люди совершают покупки на эмоциях - будьте уверены - они влюбятся в Вашу квартиру.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Я возьму на себя всю заботу по юридическому оформлению сделки, подготовке и сбору необходимых документов.
					</h5>

					<h5>
						<i class="fas fa-check"></i> Организую сделку и безопасные расчеты, помогу Вам с переездом.
					</h5>

					<h5>На протяжении всего периода продажи квартиры я буду регулярно отчитываться Вам о всех важных событиях, связанных с ходом её продажи. Буду представлять Ваши интересы на переговорах с потенциальными покупателями, решать маркетинговые, технические и организационные задачи по её продаже.</h5>

					<h5>
						<div>Я буду с Вами на связи постоянно – как на период продажи квартиры, так и на протяжении дальнейшей жизни. Я СВОИХ НЕ БРОСАЮ!</div>
						<br>
						<div>
							<strong>Звоните</strong>, не откладывая, по телефону: <strong class="text-primary"><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
						</div>
						<div>
							<i>Звонки принимаются: <i class="far fa-clock text-danger"></i> с <strong class="text-primary">7:30</strong> до <strong class="text-primary">23:00</strong></i>.
						</div>
					</h5>


					<div class="col animate-box fadeInUp animated">
						<div class="price-box">
							<h3>Закажите бесплатную предварительную оценку вашей недвижимости</h3>
							
							<input type="hidden" name="url" value="/mail/sale">

							<div class="row justify-content-center">
								<div class="col-md-6 to-animate fadeInUp animated align-self-center">

									<div class="form-group ">
										<label for="name" class="sr-only">Имя</label>
										<input name="name" class="form-control" placeholder="*Имя" type="text">
									</div>

									<div class="form-group ">
										<label for="email" class="sr-only">Email</label>
										<input name="email" class="form-control" placeholder="Email" type="email">
									</div>

									<div class="form-group ">
										<label for="phone" class="sr-only">Телефон</label>
										<input name="phone" class="form-control" placeholder="*Телефон" type="text">
									</div>

									<div class="form-group ">
										<label for="message" class="sr-only">Сообщение</label>
										<textarea name="message" cols="30" rows="5" class="form-control" placeholder="*Сообщение"></textarea>
									</div>

									<div class="form-group ">
										<button class="btn btn-primary btn-lg"  type="submit" id="sendBtn">
											Заказать <i class="far fa-paper-plane"></i>
										</button>
									</div>
									<small class="text-danger" style="margin-left: 20px;" id="statusMessage">{{ (isset($status) and !empty($status)) ? $status : '' }}</small>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>


		</div>
    </section>
@endsection
