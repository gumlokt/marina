	<section class="animated" data-section="about" id="realtor">
		<div class="container">
		    <div class="row text-secondary">
				<div class="col">
					<img src="/images/trust-realtor-logo.png" class="rounded mx-auto d-block float-left to-animate fadeInUp animated" alt="trust-realtor-logo">
				</div>

				<div class="col">
		            <h2 class="mb-4 to-animate fadeInDown animated">Сведения обо мне в Федеральной Базе Профессиональных Риэлторов</h2>

					<blockquote class="blockquote">
						<a href="https://мой-риэлтор.рф/?q=79533801100&h=4d7ba95eb256fa4c485e4396d0376cca" class="btn btn-outline-info to-animate fadeInDown animated" target="_blank">
							Просмотреть <i class="fas fa-external-link-alt fa-fw text-danger"></i>
						</a>
					</blockquote>
		        </div>
		    </div>
		</div>
	</section>
	<div class="slant-realtor"></div>
