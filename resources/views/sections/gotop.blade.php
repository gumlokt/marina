    <section class="animated" data-section="gotop" id="gotop">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<a href="#" class="gotop js-gotop"><i class="fas fa-arrow-up"></i></a>
				</div>
			</div>
		</div>
    </section>
