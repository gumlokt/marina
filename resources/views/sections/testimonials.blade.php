    <section class="animated" data-section="testimonials" id="marina-testimonials">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate fadeInUp animated">Отзывы</h2>
					<h3>РЕАЛЬНЫЕ ОТЗЫВЫ БЛАГОДАРНЫХ КЛИЕНТОВ</h3>
				</div>
			</div>

			<div class="row">
				<div class="col">

					<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200 }'>
						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide01.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide01.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide02.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide02.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide03.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide03.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide04.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide04.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide05.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide05.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide06.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide06.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="testimonials" href="/images/testimonials/slide07.jpg">
								<img class="img-thumbnail shadow" src="/images/testimonials/slide07.jpg">
							</a>
						</div>
					</div>

				</div>
			</div>





			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate fadeInUp animated">КВАЛИФИКАЦИЯ</h2>
					<h3>КАЧЕСТВО МОИХ УСЛУГ ПОДТВЕРЖДЕНО СЕРТИФИКАТАМИ</h3>
				</div>
			</div>

			<div class="row">
				<div class="col">

					<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200 }'>
						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide01.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide01.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide02.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide02.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide03.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide03.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide04.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide04.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide05.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide05.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide06.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide06.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide07.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide07.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide08.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide08.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide09.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide09.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide10.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide10.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide11.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide11.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide12.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide12.jpg">
							</a>
						</div>

						<div class="grid-item">
							<a data-fancybox="certificates" href="/images/certificates/slide13.jpg">
								<img class="img-thumbnail shadow" src="/images/certificates/slide13.jpg">
							</a>
						</div>
					</div>

				</div>
			</div>


		</div>
    </section>
