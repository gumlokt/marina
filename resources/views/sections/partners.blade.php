	<section class="animated" data-section="partners" id="partners">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">Мои партнеры</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-building fa-4x bounceIn animated"></i>
					<h3>Застройщики</h3>
					<p>Я сумела наладить систему партнерских отношений с застройщиками. Поэтому, покупая новостройку с моей помощью, Вы сэкономите на дополнительных расходах. Цена на квартиру часто становится даже меньше, чем при обращении к застройщикам напрямую. При этом вы получите полную информацию и непредвзятое отношение. Надежность – 100%.</p>
				</div>
				
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-university fa-4x bounceIn animated"></i>
					<h3>Банки</h3>
					<p>Мною заключены договора на персональное обслуживание во всех банках, выдающих ипотечные кредиты. Благодаря этому Вы получите сниженную процентную ставку на столько, на сколько это возможно, а также быстрый срок рассмотрения заявки на ипотеку, гарантированное одобрение и персонального менеджера.</p>
				</div>	
				
				<div class="clearfix visible-sm-block"></div>
				
				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-city fa-4x bounceIn animated"></i>
					<h3>Госучреждения</h3>
					<p>Я обладаю знаниями и богатым опытом того, как максимально быстро и со 100%-ным результатом оформить все необходимые документы, избавив Вас от пустой траты времени на простаивании в очередях.</p>
				</div>

				<div class="col-md-6 col-sm-6 service to-animate fadeInUp animated">
					<i class="icon fas fa-hands-helping fa-4x bounceIn animated"></i>
					<h3>Агентства</h3>
					<p>Мои партнеры – это надежные агентства не только Москвы, но и регионов, входящие в <a href="http://rgr.ru/" target="_blank">Российскую гильдию риелторов</a>. Это поможет продать Вашу недвижимость дороже, а купить и оформить – безопасно!</p>
				</div>
			</div>
		</div>
	</section>
