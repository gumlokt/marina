	<section class="intro animated" id="intro">
		<div class="container">
			<div class="row">
				<div class="col section-heading text-center">
					<h2 class="to-animate fadeInUp animated">Мои услуги</h2>
				</div>
			</div>

			<div class="row row-bottom-padded-lg">
				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_7.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-store-alt fa-3x fa-fw"></i>
						<h2>Продажа</h2>
						<p>Ваша квартира - это Ваш финансовый актив! С моей помощью Вы сможете его сберечь и приумножить!</p>
						<p><a href="/sale" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>

				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_8.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-shopping-bag fa-3x fa-fw"></i>
						<h2>Покупка</h2>
						<p>Ваша задача купить лучшую квартиру из возможных по приемлемой цене и не переплатить? Вам ко мне!</p>
						<p><a href="/purchase" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>

				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_10.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-exchange-alt fa-3x fa-fw"></i>
						<h2>Обмен</h2>
						<p>У вас сложный объект, который трудно продать? Предлагаю Вам рассмотреть обмен на что-то более ликвидное!</p>
						<p><a href="/exchange" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>
			</div>


			<div class="row row-bottom-padded-lg">
				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_7.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-file-alt fa-3x fa-fw"></i>
						<h2>Оформление документов</h2>
						<p>Независимо от того, продаете вы или покупаете, всегда очень важно правильно оформить документы!</p>
						<p><a href="/paperwork" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>

				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_8.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-shield-alt fa-3x fa-fw"></i>
						<h2>Безопасная сделка</h2>
						<p>Хотите купить или продать недвижимость безопасно? Будьте уверены, я Вам с этим помогу!</p>
						<p><a href="/safedeal" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>

				<div class="marina-block to-animate fadeInRight animated" style="background-image: url(images/img_10.jpg);">
					<div class="overlay-darker"></div>
					<div class="overlay"></div>
					<div class="marina-text">
						<i class="marina-intro-icon fas fa-money-bill fa-3x fa-fw"></i>
						<h2>Выгодная ипотека</h2>
						<p>Рассматриваете ипотеку? Позвоните мне - я помогу Вам выбрать кредит на самых выгодных для Вас условиях!</p>
						<p><a href="/mortgage" class="btn btn-primary">Подробнее <i class="fas fa-angle-double-right fa-fw text-danger"></i></a></p>
					</div>
				</div>
			</div>
		</div>
	</section>