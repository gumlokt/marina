	<section class="animated" data-section="home" data-stellar-background-ratio="0.5" id="home">
		<div class="container">
			<div class="row text-white">
				<div class="col-lg-6">
					<h1 class="to-animate fadeInDown animated">ЭКСПЕРТ ПО НЕДВИЖИМОСТИ</h1>
					<h2 class="to-animate fadeInDown animated">МАРИНА МОРОЗОВА</h2>
					<h2 class="mt-4 to-animate fadeInDown animated">Всегда на связи</h2>
					<h2 class="mb-4 to-animate fadeInDown animated">
						<!-- <i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00 -->
						<strong><i class="fas fa-mobile-alt fa-fw text-danger"></i> +7 (953) 380 11 00</strong>
					</h2>

					<blockquote class="blockquote text-right to-animate fadeInUp animated">
						<p class="mb-0 text-light">
							Квартира для человека - это основа основ.
							Её покупка, продажа, обмен влияют на всю нашу жизнь!
							Решением таких непростых задач должны заниматься специалисты.
							Обратившись ко мне, Вы сможете в полной мере оценить качество работы профессионального риэлтора!
						</p>
					</blockquote>
				</div>

				<div class="col">
					<img src="/images/marina_transparent.png" class="rounded float-right to-animate fadeInUp animated" alt="my photo">
				</div>
			</div>
		</div>

		<div class="slant-home"></div>
	</section>
