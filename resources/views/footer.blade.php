    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <span class="text-light">&copy; {{ '2019' == date('Y') ? 2019 : '2019-' . date('Y') }} <span class="text-danger">Марина Морозова</span></span>
                </div>
                <div class="col text-right">
                    <span class="text-light">Эксперт по недвижимости</span>
                </div>
            </div>
        </div>
    </footer>
