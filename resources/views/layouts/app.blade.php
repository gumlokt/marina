<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ url('favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700&display=swap&subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700&display=swap" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet" type="text/css">
</head>
    <body>
        <!-- Yandex.Metrika counter -->
        <!-- /Yandex.Metrika counter -->

        @include('navbar')
        @yield('content')
        @include('footer')

        <script src="{{ url('js/app.js') }}"></script>
        <script src="{{ url('js/fancybox.min.js') }}"></script>
        <script src="js/stellar.min.js"></script>
        <script src="js/masonry.min.js"></script>
        <script src="{{ url('js/settings.js') }}"></script>
        @yield('customJs')
    </body>
</html>
