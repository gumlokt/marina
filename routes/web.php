<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'IndexController@index');


// Nav bar links

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/contacts', function () {
    return view('pages.contacts');
});


// Intro links

Route::get('/sale', function () {
    return view('pages.sale');
});

Route::get('/purchase', function () {
    return view('pages.purchase');
});

Route::get('/exchange', function () {
    return view('pages.exchange');
});


Route::get('/paperwork', function () {
    return view('pages.paperwork');
});

Route::get('/safedeal', function () {
    return view('pages.safedeal');
});

Route::get('/mortgage', function () {
    return view('pages.mortgage');
});


// Mailing

Route::post('/mail/consultation', 'MailingController@consultation');
Route::post('/mail/purchase', 'MailingController@purchase');
Route::post('/mail/sale', 'MailingController@sale');

